using UnityEngine;
using System.Collections;

public class Game : MonoBehaviour {
	private PlayerCarController player = null;
	[SerializeField] public bool Paused = false;
	[SerializeField] public bool raceStart = false;
	
	// Use this for initialization
	void Start () {	
		this.player = (PlayerCarController)FindObjectOfType(typeof(PlayerCarController));
	}
	
	// Update is called once per frame
	void Update () {
		
		if(Input.GetKeyDown(KeyCode.Escape)) {
			if(!this.Paused) {
				/*
				Object[] objects = FindObjectsOfType(typeof(GameObject));
				
				foreach(GameObject gameobject in objects)
					gameobject.SendMessage("OnPauseGame", SendMessageOptions.DontRequireReceiver);
				*/
				this.Paused = true;
				Time.timeScale = 0;
			} else {
				/*
				Object[] objects = FindObjectsOfType(typeof(GameObject));
				
				foreach(GameObject gameobject in objects)
					gameobject.SendMessage("OnResumeGame", SendMessageOptions.DontRequireReceiver);
				*/
				this.Paused = false;
				Time.timeScale = 1;
			}
		}
		
		if(this.player.win) {
			if(Input.GetKeyDown(KeyCode.Escape))
				Application.Quit();
		}
	}
}
