using UnityEngine;
using System.Collections;

public class WheelSpin : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		float carSpeed = this.transform.parent.parent.rigidbody.velocity.magnitude;
		this.transform.Rotate(carSpeed*Time.deltaTime*100, 0, 0);
	}
}
