using UnityEngine;
using System.Collections;

public class DrawWaypoints : MonoBehaviour {

	void OnDrawGizmos() {
		foreach(Transform child in this.transform) {
		 	if(child.gameObject.GetComponent<Waypoint>())
				Gizmos.color = Color.red;
				Gizmos.DrawSphere(child.position, 1.0f);
		}
	}
}
